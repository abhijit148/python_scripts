#This is a python script to send an HTML email with multiple attachments 
#through the Gmail server using Python.
#Author: Abhijit Agarwal - abhijit.148 at gmail dot com
#Sources have been mentioned in comments wherever applicable.

import smtplib #http://docs.python.org/2/library/smtplib.html
import os
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.MIMEBase import MIMEBase
from email import Encoders
from email.Utils import COMMASPACE, formatdate

#From and To Email Addresses go here
fromaddr = "sender@emailhost.com"
toaddrs  = ["receiver@emailhost.com"]

subject="An HTML Email with Attachments "

#Email Body:Plain Text and HTML
text = "Hi!\nHow are you?\nHere is the link you wanted:\nhttp://www.python.org."
html = """
	<html>
	  <head></head>
	  <body>
	    <p>Hi!<br>
	       How are you?<br>
	       Here is the <a href="http://www.python.org">link</a> you wanted.
	       Also find two files attached.
	    </p>
	  </body>
	</html>
	"""

#Email Credentials
username = 'username@emailhost.com'
password = 'password' 

#Files to be attached
files=['attachment.ext', 'attachment2.ext']

#The function to send the email
def send_mail(fromaddr, toaddrs, subject, text, html, username, password, files=[],host='smtp.gmail.com:587'):
	assert type(toaddrs)==list
	assert type(files)==list

	#http://stackoverflow.com/questions/882712/sending-html-email-using-python
	# Create message container - the correct MIME type is multipart/alternative.
	msg = MIMEMultipart('alternative')
	msg['Subject'] = subject
	msg['From'] = fromaddr
	msg['To'] = COMMASPACE.join(toaddrs)
	msg['Date'] = formatdate(localtime=True)

	#Attaching Files: http://dzone.com/snippets/send-email-attachments-python
	for f in files:
		part = MIMEBase('application', "octet-stream")
		part.set_payload( open(f,"rb").read() )
		Encoders.encode_base64(part)
		part.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(f))
		msg.attach(part)

	# Record the MIME types of both parts - text/plain and text/html.
	part1 = MIMEText(text, 'plain')
	part2 = MIMEText(html, 'html')

	# Attach parts into message container.
	# According to RFC 2046, the last part of a multipart message, in this case
	# the HTML message, is best and preferred.
	msg.attach(part1)
	msg.attach(part2)

	#Connecting to the server and logging in 
	#http://www.nixtutor.com/linux/send-mail-through-gmail-with-python/	 
	server = smtplib.SMTP(host)
	server.starttls()
	server.login(username,password)
	server.sendmail(fromaddr, toaddrs, msg.as_string())  
	server.quit()
	print "Mail sent successfully"

#Calling the Function
send_mail(fromaddr, toaddrs, subject, text, html, username, password, files)